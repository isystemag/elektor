# Debugging the Arduino Zero & M0 Pro #
## Delving deeper into the world of Arduino ##

Firstly, thanks for stopping by at our BitBucket repository that supports the
article published in Elektor Magazine. Here you will find everything you need to
get started debugging on the Arduino M0 Pro or Genuino/Arduino Zero boards. Just
follow the instructions below to download this repository to your development PC
and open the tutorials included in the package.

### How do I get set up? ###

The first step is to download this repository to your development PC. If you are
a git user, you most likely know how to pull the repository. If you do not use
git, you can simply download the repository as follows:

* Click the [download link](http://isy.si/2) 
on the left-hand side of this page (the cloud icon with the arrow pointing 
down).
* On the "Downloads" page, click the "Download repository" link.
* The download is a zipped package of the contents of the repository. Unzip this
file to suitable location.
* Start by open the folder named "Tutorials" and open the file "Tutorial-1.pdf"
in a PDF reader of your choosing.

The first two tutorials take you through all the necessary steps to install the 
software needed for all the topics covered.

### What software will I need? ###

The first and second tutorials (Tutorial-1.pdf and Tutorial-2.pdf) cover the 
software needed and how to install it. If you'd liked to know in advance what is
expected, this list provides you with the information you need.

* [winIDEA Open](http://isy.si/winideaopen) - This is the Integrated Development
Environment (IDE) used to program and debug the Arduino board
* Arduino IDE - This is required so as the source files for the Arduino 
environment are available for your sketch. Use either:
    * [Arduino M0 Pro IDE](http://isy.si/arduino-org) - from arduino.org
    * [Genuino/Arduino Zero IDE](http://isy.si/arduino-cc) - from arduino.cc
* [MinGW](http://isy.si/2) - Required to provide some Unix-world tools 
to build the Arduino sketches automatically (specifically the 'make' utility)
* [git for Windows](http://isy.si/windows-git) (optional) - Used to pull this 
repository to your PC


### Who do I talk to? ###

If you would like to contact the author, please write to Stuart Cording at 
inews@isystem.com
We look forward to hearing your feedback!